import { Component, OnInit } from '@angular/core';
import { ServiseService } from '../services/servise.service';
import { Entity } from '../services/entity';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  player: any = [];

  valueForm: FormGroup = new FormGroup({
    playerName: new FormControl(),
  });
  constructor(public serviseService: ServiseService) {}

  ngOnInit(): void {
    this.serviseService.getAll().subscribe((data: Entity[]) => {
      this.player = data;
      console.log(this.player);
    });
  }

  // tslint:disable-next-line:typedef
  delete(playerId: number) {
    this.serviseService.delete(playerId).subscribe(res => {
      this.player = this.player.filter((item: { playerId: number; }) => item.playerId !== playerId);
      console.log('Post deleted successfully!');
    });
  }
  // tslint:disable-next-line:typedef
  search() {
    return this.serviseService.search(this.valueForm.value.playerName).subscribe(value =>
   this.player = value,
    )}
}

