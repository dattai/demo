import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Entity} from '../services/entity';
import {ServiseService} from '../services/servise.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  playerId!: number;
  entity!: Entity;
  form = new FormGroup({
    playerId: new FormControl(),
    playerName: new FormControl(),
    gender: new FormControl(),
    birthday: new FormControl(),
    address: new FormControl(),
    teamName: new FormControl(),
    positionPlay: new FormControl(),
    transportCost: new FormControl(),
  });
  constructor(
    public sv: ServiseService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  // @ts-ignore
  // @ts-ignore
  ngOnInit(): void {
    this.playerId = this.route.snapshot.params.playerId;
    this.sv.find(this.playerId).subscribe((data: Entity) => {
      this.entity = data;
      // @ts-ignore
      this.form = new FormGroup({
        playerId: new FormControl(data.playerId),
        playerName: new FormControl(data.playerName),
        gender: new FormControl(data.gender),
        birthday: new FormControl(data.birthday),
        address: new FormControl(data.address),
        teamName: new FormControl(data.teamName),
        positionPlay: new FormControl(data.positionPlay),
        transportCost: new FormControl(data.transportCost),
      });
      console.log(this.entity);
    });
  }
  // tslint:disable-next-line:typedef
  submit(){
    console.log(this.form.value);
    this.sv.update(this.form.value).subscribe((res: any) => {
      console.log(res);
      this.router.navigateByUrl('/');
    });
  }
}
