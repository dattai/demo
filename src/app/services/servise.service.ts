import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Entity } from './entity';

@Injectable({
  providedIn: 'root'
})
export class ServiseService {

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  getAll(): Observable<Entity[]> {
    return this.httpClient.get<Entity[]>('player_services/getListPlayer/')
      .pipe(
        catchError(this.errorHandler)
      );
  }

  create(entity: any): Observable<Entity> {
    return this.httpClient.post<Entity>('/player_services/insertPlayer/', JSON.stringify(entity), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  find(playerId: number): Observable<Entity> {
    return this.httpClient.get<Entity>('/player_services/getPlayerById/' + playerId)
      .pipe(
        catchError(this.errorHandler)
      );
  }
  search(playerName: string): Observable<Entity> {
    return this.httpClient.get<Entity>('/player_services/getPlayerByName/' + playerName);
  }


  update( entity: any): Observable<Entity> {
    return this.httpClient.put<Entity>('/player_services/updatePlayer/', JSON.stringify(entity), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  delete(playerId: number){
    return this.httpClient.delete('/player_services/deletePlayer/' + playerId, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }
  // tslint:disable-next-line:typedef
  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
