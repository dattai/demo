export interface Entity {
  playerId: number;
  playerName: string;
  // tslint:disable-next-line:ban-types
  gender: Boolean;
  birthday: Date;
  address: string;
  teamName: string;
  positionPlay: string;
  transportCost: number;
}
