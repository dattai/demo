
import { Component, OnInit } from '@angular/core';
import {  FormControl, FormGroup } from '@angular/forms';
import { ServiseService } from '../services/servise.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements  OnInit{
  form!: FormGroup;
  ngOnInit(): void {
    this.form = new FormGroup({
      playerName: new FormControl(),
      gender: new FormControl(),
      birthday: new FormControl(),
      address: new FormControl(),
      teamName: new FormControl(),
      positionPlay: new FormControl(),
      transportCost: new FormControl(),
    });
  }
  constructor(public serviseService: ServiseService, private router: Router) { }
  // tslint:disable-next-line:typedef
  submit(){
    console.log(this.form.value);
    this.serviseService.create(this.form.value).subscribe((res: any) => {
      console.log('Post created successfully!');
      this.router.navigateByUrl('/');
    });
  }
}
